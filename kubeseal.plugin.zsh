install_kubeseal() {
    local whereami="$1"
    local version="$2"
    local url="https://github.com/bitnami-labs/sealed-secrets/releases/download/v$version/kubeseal-$version-linux-amd64.tar.gz"
    local bin="$HOME/.local/bin"
    local completions_dir="$whereami/src"

    if ! type kubeseal 2>&1 >/dev/null; then
        mkdir -p "$bin"
        curl -L "$url" >"$whereami/kubeseal.tar.gz"
        tar -xvzf "$whereami/kubeseal.tar.gz" --directory "$whereami"
        chmod +x "$whereami/kubeseal"
        cp -v "$whereami/kubeseal" "$bin"
    fi
}

install_kubeseal "${0:A:h}" "0.18.2"
unset install_kubeseal
